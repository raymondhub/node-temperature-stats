TEMPERATURE STATS APP

DESIGN DECISIONS:
1. Designed with SOLID Principles in mind
2. Modular approach for the ease of maintainability and scalability
3. Clean and well-documented source code linted by standardjs: Use JavaScript Standard Style (https://standardjs.com)
4. Use node stream to handle huge data input/file - memory efficiency (validated node memory via Activity Monitor (MAC))

ASSUMPTIONS:

1. Non-numeric values will be removed, and string numbers will be converted to Numbers for all given temprature values
2. The number of fridge sensors is undisclosed prior to analysing the data

MINIMUM REQUIREMENTS:

1. Node 6.12.0 or above

HOW TO SET UP, RUN AND TEST THE APP VIA THE COMMAND LINE (LINUX/MAC):

1. Git clone this repository into your local machine: git clone git@bitbucket.org:raymondhub/temperature-stats.git
2. run npm install
3. Get into the cloned folder: cd temprature-stats
4. Generate new sample data (optional) : npm run sample
5. Run the app: npm run prod
6. Once the app is up and running, the instruction/question is prompted on the command line and please provide the following file path as an answer:
   tests/inputs/input-example.json
   or
   tests/inputs/inputs.json (if sample data has been generated in step 4)

The example of the running app:
raymonds-Mac:temperature-stats raymond$ node tests/sample-data.js // generate sample data in tests/inputs/inputs.json
raymonds-Mac:temperature-stats raymond$ node app
Please provide the file path for JSON data input (e.g: tests/inputs/input-example.json): tests/inputs/inputs.json
Validating and processing......
The results are being displayed below:

[{"id":"a","average":2.91,"median":2.87,"mode":[2.1]},
{"id":"b","average":3.02,"median":2.98,"mode":[2.28,2.87,2.9,3.05]},
{"id":"d","average":3.02,"median":3.06,"mode":[3.81,3.55,3.44,2.75,2.7,2.47]},
{"id":"c","average":3.05,"median":3.04,"mode":[2.33,3.66]}]


Thank you for submitting your data and the result has also been saved in this JSON file: temprature-stats/tests/outputs/outputs.json
raymonds-MAC:temperature-stats raymond$
