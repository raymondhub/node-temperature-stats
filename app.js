const path = require('path')
const fs = require('fs')
const readline = require('readline')
const stats = require('./src/math-stats')

// let inputFile = process.argv[2]
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question('\nPlease provide the file path for JSON data input (e.g: tests/inputs/input-example.json): ', (inputFile) => {
  console.log(`Validating and processing......`)
  let fileData = ''
  let readStream = fs.createReadStream(path.resolve(__dirname, inputFile), 'utf8')
    // Listen to handle the error just in case it gets emmited
  readStream.on('error', (err) => {
    console.log(`Error when validating the input: ${err}`)
  })
  readStream.on('data', (chunk) => {
    fileData += chunk
  }).on('end', () => {
    let sensorIds = []
    let sortedDataObj = {}
        // Collect all available sensor ids
    for (let data of JSON.parse(fileData)) {
      sensorIds.push(data.id)
    }
        // sensorIds = [...new Set(sensorIds)]
        // unique ids only in  the array
    sensorIds = Array.from(new Set(sensorIds))
    for (let id of sensorIds) {
      sortedDataObj[id] = []
    }
    for (let data of JSON.parse(fileData)) {
      let id = data.id
      sortedDataObj[id].push(data.temperature)
    }
        // console.log(sortedDataObj)
    const resultFile = path.resolve(__dirname, 'tests/outputs/outputs.json')
    const result = fs.createWriteStream(resultFile, 'utf8')
    result.write(`[`)
    let countKeys = 0
    let lastKey = Object.keys(sortedDataObj).length - 1
    for (const sensor in sortedDataObj) {
      let cleanResults = stats.numbers(sortedDataObj[sensor])
      if (countKeys === lastKey) {
        result.write(`{"id":"${sensor}","average":${stats.mean(cleanResults).toFixed(2)},"median":${stats.median(cleanResults).toFixed(2)},"mode":[${stats.mode(cleanResults)}]}]\n`)
      } else {
        result.write(`{"id":"${sensor}","average":${stats.mean(cleanResults).toFixed(2)},"median":${stats.median(cleanResults).toFixed(2)},"mode":[${stats.mode(cleanResults)}]},\n`)
      }
      countKeys++
    }

    result.on('finish', () => {
      console.log(`The results are being displayed below:\n`)
      let resultData = ''
      let readResultStream = fs.createReadStream(resultFile, 'utf8')
      readResultStream.on('error', (err) => {
        console.log(`Error when reading the result: ${err}`)
      })
      readResultStream.on('data', (chunk) => {
        resultData += chunk
      }).on('end', () => {
        console.log(resultData)
        console.log(`\nThank you for submitting your data and the result has also been saved in this JSON file: ${resultFile}\n`)
      })
    })
    result.end()
  })

  rl.close()
})
