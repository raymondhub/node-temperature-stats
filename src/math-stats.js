'use strict'

/*
 * Validate if the input is a number.
 * @param n
 */
let _isNumber = (val) => {
  return !isNaN(parseFloat(val)) && isFinite(val)
}

/*
 * Sorting the array into numbers only array.
 * @param vals
 */
let numbers = (vals) => {
  let nums = []
  if (vals == null) return nums

  for (let i = 0; i < vals.length; i++) {
    if (_isNumber(vals[i])) { nums.push(vals[i]) }
  }
  return nums
}

/*
 * sorting the array in ascending numerical order
 * @param vals
 */
let _nsort = (vals) => {
  return vals.sort(function numericSort (a, b) { return a - b })
}

/*
 * Total sum
 *@param vals
 */
let _sum = (vals) => {
  return vals.reduce((sum, value) => sum + value, 0)
}

/*
 * Mean or the average is found by adding up all of the given data and dividing by the number of data entries.
 *@param vals
 */
let mean = (vals) => {
  if (vals.length === 0) return NaN
  return (_sum(vals) / vals.length)
}

/*
 * Median is the middle value in the "sorted" list of numbers
 *@param vals
 */
let median = (vals) => {
  if (vals.length === 0) return NaN
  let half = (vals.length / 2) | 0
  vals = _nsort(vals)
  if (vals.length % 2) {
    return vals[half]
  } else {
    return (vals[half - 1] + vals[half]) / 2.0
  }
}

/*
 * Mode is the numbers occuring most often
 *@param vals
 */
let mode = (vals) => {
  let mode = NaN
  if (vals.length === 0) return mode
  let dist = {}

  for (let i = 0; i < vals.length; i++) {
    let value = vals[i]
    let me = dist[value] || 0
    me++
    dist[value] = me
  }

  let rank = numbers(Object.keys(dist).sort(function sortMembers (a, b) { return dist[b] - dist[a] }))
  mode = rank[0]
  if (dist[rank[1]] === dist[mode]) {
    if (rank.length === vals.length) return vals
    let modes = new Set([mode])
    let modeCount = dist[mode]
    for (let i = 1; i < rank.length; i++) {
      if (dist[rank[i]] === modeCount) {
        modes.add(rank[i])
      } else {
        break
      }
    }
    return Array.from(modes)
  }
  return mode
}

module.exports.numbers = numbers
module.exports.mean = mean
module.exports.median = median
module.exports.mode = mode
