'use strict'
const fs = require('fs')
const path = require('path')
/* Whenever this script is being run by tests/sample-data.js -> node tests/sample-data.js:
1. It creates new file/overwrite the existing input file (inputs.json)
2. It populates the file with new sample data
*/
let _timestamp = Date.now()
// Generate random id picked out of the array of ids
// let _sensorIds = ['a','b','c','d','e','f','g','h','i']
let _sensorIds = ['a', 'b', 'c', 'd']
// Generate random sensor id
let _getRandomId = () => {
  return _sensorIds[Math.floor(Math.random() * _sensorIds.length)]
}
// Generate random temp between/equal two values eg. between 2 and 4
let _tempValues = (min = 2, max = 4) => {
  return (Math.random() * (max - min) + min).toFixed(2) // max 2 decimal points
}
// numOfData = 1e6 //eg. this is massive 1 mill
let data = (numOfData = 1000, temValuesMin = 2, tempValuesMax = 4) => {
  const file = fs.createWriteStream(path.resolve(__dirname, '../tests/inputs/inputs.json'))
  file.write(`[`)
  for (let i = 0; i <= numOfData; i++) {
    i === numOfData ? file.write(`{"id":"${_getRandomId()}","timestamp":${_timestamp},"temperature":${_tempValues(temValuesMin, tempValuesMax)}}]`)
                      : file.write(`{"id":"${_getRandomId()}","timestamp":${_timestamp},"temperature":${_tempValues(temValuesMin, tempValuesMax)}},\n`)
  }
  file.end()
}

module.exports.data = data
